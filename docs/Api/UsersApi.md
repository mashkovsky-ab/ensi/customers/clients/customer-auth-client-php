# Ensi\CustomerAuthClient\UsersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UsersApi.md#createUser) | **POST** /users | Создание объекта типа User
[**deleteUser**](UsersApi.md#deleteUser) | **DELETE** /users/{id} | Удаление объекта типа User
[**getCurrentUser**](UsersApi.md#getCurrentUser) | **POST** /users:current | Получить текущего пользователя
[**getUser**](UsersApi.md#getUser) | **GET** /users/{id} | Получение объекта типа User
[**passwordReset**](UsersApi.md#passwordReset) | **POST** /users/password-reset-first | Сброс пароля
[**passwordResetSecond**](UsersApi.md#passwordResetSecond) | **POST** /users/password-reset-second | Сброс пароля
[**patchUser**](UsersApi.md#patchUser) | **PATCH** /users/{id} | Обновления части полей объекта типа User
[**refreshPasswordToken**](UsersApi.md#refreshPasswordToken) | **POST** /users/{id}:refresh-password-token | Обновление токена для установки пароля
[**searchOneUser**](UsersApi.md#searchOneUser) | **POST** /users:search-one | Поиск объекта типа User
[**searchUsers**](UsersApi.md#searchUsers) | **POST** /users:search | Поиск объектов типа User



## createUser

> \Ensi\CustomerAuthClient\Dto\UserResponse createUser($create_user_request)

Создание объекта типа User

Создание объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_user_request = new \Ensi\CustomerAuthClient\Dto\CreateUserRequest(); // \Ensi\CustomerAuthClient\Dto\CreateUserRequest | 

try {
    $result = $apiInstance->createUser($create_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->createUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_user_request** | [**\Ensi\CustomerAuthClient\Dto\CreateUserRequest**](../Model/CreateUserRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteUser

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse deleteUser($id)

Удаление объекта типа User

Удаление объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteUser($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->deleteUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCurrentUser

> \Ensi\CustomerAuthClient\Dto\UserResponse getCurrentUser()

Получить текущего пользователя

Поиск объектов типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Ensi\CustomerAuthClient\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCurrentUser();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getCurrentUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getUser

> \Ensi\CustomerAuthClient\Dto\UserResponse getUser($id, $include)

Получение объекта типа User

Получение объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getUser($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## passwordReset

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse passwordReset($password_reset_first_request)

Сброс пароля

Сброс пароля (отправка кода подтверждения на почту/номер телефона)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$password_reset_first_request = new \Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest(); // \Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest | 

try {
    $result = $apiInstance->passwordReset($password_reset_first_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->passwordReset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password_reset_first_request** | [**\Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest**](../Model/PasswordResetFirstRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## passwordResetSecond

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse passwordResetSecond($password_reset_second_request)

Сброс пароля

Сброс пароля

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$password_reset_second_request = new \Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest(); // \Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest | 

try {
    $result = $apiInstance->passwordResetSecond($password_reset_second_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->passwordResetSecond: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password_reset_second_request** | [**\Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest**](../Model/PasswordResetSecondRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchUser

> \Ensi\CustomerAuthClient\Dto\UserResponse patchUser($id, $patch_user_request)

Обновления части полей объекта типа User

Обновления части полей объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_user_request = new \Ensi\CustomerAuthClient\Dto\PatchUserRequest(); // \Ensi\CustomerAuthClient\Dto\PatchUserRequest | 

try {
    $result = $apiInstance->patchUser($id, $patch_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->patchUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_user_request** | [**\Ensi\CustomerAuthClient\Dto\PatchUserRequest**](../Model/PatchUserRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## refreshPasswordToken

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse refreshPasswordToken($id)

Обновление токена для установки пароля

Обновить токен для установки пароля

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->refreshPasswordToken($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->refreshPasswordToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneUser

> \Ensi\CustomerAuthClient\Dto\UserResponse searchOneUser($search_users_request)

Поиск объекта типа User

Поиск объектов типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_users_request = new \Ensi\CustomerAuthClient\Dto\SearchUsersRequest(); // \Ensi\CustomerAuthClient\Dto\SearchUsersRequest | 

try {
    $result = $apiInstance->searchOneUser($search_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchOneUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_users_request** | [**\Ensi\CustomerAuthClient\Dto\SearchUsersRequest**](../Model/SearchUsersRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUsers

> \Ensi\CustomerAuthClient\Dto\SearchUsersResponse searchUsers($search_users_request)

Поиск объектов типа User

Поиск объектов типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_users_request = new \Ensi\CustomerAuthClient\Dto\SearchUsersRequest(); // \Ensi\CustomerAuthClient\Dto\SearchUsersRequest | 

try {
    $result = $apiInstance->searchUsers($search_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_users_request** | [**\Ensi\CustomerAuthClient\Dto\SearchUsersRequest**](../Model/SearchUsersRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\SearchUsersResponse**](../Model/SearchUsersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

