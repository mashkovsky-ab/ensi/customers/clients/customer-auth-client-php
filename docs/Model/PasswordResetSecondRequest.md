# # PasswordResetSecondRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reset_type** | **string** | Способ сброса пароля (телефон/email) | [optional] 
**code** | **string** | Код подтверждения или password-token | [optional] 
**email** | **string** | Email пользователя | [optional] 
**phone** | **string** | Номер телефона пользователя | [optional] 
**password** | **string** | Пароль | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


