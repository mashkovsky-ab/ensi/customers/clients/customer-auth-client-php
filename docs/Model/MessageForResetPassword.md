# # MessageForResetPassword

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_full_name** | **int** | Имя и фамилия пользователя | [optional] 
**user_phone** | **string** | Номер телефона пользователя | [optional] 
**user_email** | **string** | Email пользователя | [optional] 
**token** | **string** | Токен | [optional] 
**confirmation_code** | **string** | Код подтверждения | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


