<?php

namespace Ensi\CustomerAuthClient;

class CustomerAuthClientProvider
{
    /** @var string[] */
    public static $apis = ['\Ensi\CustomerAuthClient\Api\UsersApi', '\Ensi\CustomerAuthClient\Api\OauthApi'];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CustomerAuthClient\Dto\UserReadonlyProperties',
        '\Ensi\CustomerAuthClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest',
        '\Ensi\CustomerAuthClient\Dto\PaginationTypeEnum',
        '\Ensi\CustomerAuthClient\Dto\ErrorResponse2',
        '\Ensi\CustomerAuthClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CustomerAuthClient\Dto\ErrorResponse',
        '\Ensi\CustomerAuthClient\Dto\User',
        '\Ensi\CustomerAuthClient\Dto\MessageForResetPassword',
        '\Ensi\CustomerAuthClient\Dto\ResponseBodyPagination',
        '\Ensi\CustomerAuthClient\Dto\ModelInterface',
        '\Ensi\CustomerAuthClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\CustomerAuthClient\Dto\EmptyDataResponse',
        '\Ensi\CustomerAuthClient\Dto\RequestBodyPagination',
        '\Ensi\CustomerAuthClient\Dto\MessageForSetPassword',
        '\Ensi\CustomerAuthClient\Dto\CreateUserRequest',
        '\Ensi\CustomerAuthClient\Dto\SearchUsersResponse',
        '\Ensi\CustomerAuthClient\Dto\UserWriteOnlyProperties',
        '\Ensi\CustomerAuthClient\Dto\GrantTypeEnum',
        '\Ensi\CustomerAuthClient\Dto\CreateTokenRequest',
        '\Ensi\CustomerAuthClient\Dto\MessageForResetPasswordSuccess',
        '\Ensi\CustomerAuthClient\Dto\SearchUsersResponseMeta',
        '\Ensi\CustomerAuthClient\Dto\CreateTokenResponse',
        '\Ensi\CustomerAuthClient\Dto\PatchUserRequest',
        '\Ensi\CustomerAuthClient\Dto\Error',
        '\Ensi\CustomerAuthClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\CustomerAuthClient\Dto\SearchUsersRequest',
        '\Ensi\CustomerAuthClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CustomerAuthClient\Dto\UserResponse',
        '\Ensi\CustomerAuthClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\CustomerAuthClient\Dto\UserFillableProperties',
        '\Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CustomerAuthClient\Configuration';
}
